#include <stdio.h>
int main()
{
    int num1, num2;    
    int *ptr1, *ptr2; 
    ptr1 = &num1; 
    ptr2 = &num2; 
    printf("Enter any two real numbers: ");
    scanf("%d%d", ptr1, ptr2);
    printf("Sum = %d\n", *ptr1 + *ptr2);
    printf("Difference = %d\n", *ptr1 - *ptr2);
    printf("Product = %d\n", *ptr1 * *ptr2);
    printf("Quotient = %f\n", (float)*ptr1 / *ptr2);
    printf("remainder = %d\n",*ptr1 % *ptr2 );
    return 0;
}