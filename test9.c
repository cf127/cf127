#include <stdio.h>
struct student{    
     char name[30];
     int roll;
     float marks;
	 char sec[2];
	 char dept[10];
	 float fees;
} s[2];

int main(){
    struct student s[2];
    printf("\n Enter information of student 1\n");
    printf("Enter name: ");
    scanf("%s", s[0].name);
    printf("Enter roll number: ");
    scanf("%d", &s[0].roll);
	printf("Enter section: ");
    scanf("%s", s[0].sec);
	printf("Enter department: ");
    scanf("%s", s[0].dept);
	printf("Enter marks: ");
    scanf("%f", &s[0].marks);
	printf("Enter fees: ");
    scanf("%f", &s[0].fees);
	
	printf("\n Enter information of student 2\n");
    printf("Enter name: ");
    scanf("%s", s[1].name);
    printf("Enter roll number: ");
    scanf("%d", &s[1].roll);
	printf("Enter section: ");
    scanf("%s", s[1].sec);
	printf("Enter department: ");
    scanf("%s", s[1].dept);
	printf("Enter marks: ");
    scanf("%f", &s[1].marks);
	printf("Enter fees: ");
    scanf("%f", &s[1].fees);
	printf("\n Student details with highest marks:\n ");
	if(s[0].marks> s[1].marks)
	{
       printf("\tName:%s\n ",s[0].name);
       printf("\tRoll number: %d\n",s[0].roll);
	   printf("\tSection:%s\n ",s[0].sec);
	   printf("\tDepartment:%s\n ",s[0].dept);
       printf("\tMarks:%f\n",s[0].marks);
	   printf("\tFees :%f\n",s[0].fees);
	}
	else{
	   printf("\tName:%s\n ",s[1].name);
       printf("\tRoll number: %d\n",s[1].roll);
	   printf("\tSection:%s\n ",s[1].sec);
	   printf("\tDepartment:%s\n ",s[1].dept);
       printf("\tMarks:%f\n",s[1].marks);
	   printf("\tFees :%f\n",s[1].fees);
	}
return 0;
}
