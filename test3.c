#include<stdio.h>
#include<math.h>
int main(){
	float a,b,c,D,root1,root2,real, imag;
	printf("Enter the coefficients a,b,c:\n");
	scanf("%f%f%f",&a,&b,&c);
	D=b*b-4*a*c;
	if(D>0)
	{
	root1 = (-b + sqrt(D))/ (2*a);
    root2 = (-b - sqrt(D))/ (2*a);
    printf("root1 = %f and root2 = %f\n", root1, root2);
    }
	else if(D<0)
	{   real = -b /(2 *a);
        imag = sqrt(-D) / (2 * a);
        printf("root1 = %f+i%f and root2 = %f-i%f\n", real,imag,real,imag);
    }
	
	else if(D==0)
	{
	root1 = (-b / (2*a));
    printf("root = %f\n", root1);
    }
	return 0;
 }

