#include <stdio.h>
int main()
{
  int marks[5][3], maxMark[5], r, c;
  for(r = 0; r < 5; r++) {
    printf("Enter marks of 3 tests of Student #%d: ", (r + 1));
    for(c = 0; c < 3; c++) {
      scanf("%d", &marks[r][c]);
    }
  }
  for(r = 0; r < 5; r++) {
    maxMark[r] = 0;
    for(c = 0; c < 3; c++) {
      if (marks[r][c] > maxMark[r]) {
        maxMark[r] = marks[r][c];
      }
    }
  }
  
  for(r = 0; r < 5; r++) {
    printf("Max mark of Student #%d is %d\n", (r + 1), maxMark[r]);
  }
  return 0;
}