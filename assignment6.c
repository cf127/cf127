#include <stdio.h> 
int main() 
{ 
	int prime[10] = { 2,3,5,11,13,17,19,23 }; 
	int i, x, pos, n = 8; 
	printf("The original array of prime nos. between 1 to 25\n");
	for (i = 0; i < n; i++)
	{
		printf("%d ", prime[i]);
	}
   printf("\nEnter the missing value\n");
   scanf("%d", &x);
   printf("Enter the location where you wish to insert an element\n");
   scanf("%d", &pos);
	for (i = n-1; i>= pos-1; i--) 
		prime[i+1] = prime[i]; 
	prime[pos - 1] = x;
	printf("The updated array of prime nos.\n");
	for (i = 0; i < n; i++) 
		printf("%d ", prime[i]); 
	printf("\n"); 
	return 0; 
} 
