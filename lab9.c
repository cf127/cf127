#include <stdio.h>
void swap(int *x, int *y);
 
int main () {
    int n1, n2;
    printf("Enter value of n1: ");
    scanf("%d",&n1);
    printf("Enter value of n2: ");
    scanf("%d",&n2);

   printf("Before swap, value of n1 : %d\n", n1 );
   printf("Before swap, value of n2 : %d\n", n2 );
   swap(&n1, &n2);
   printf("After swap, value of n1 : %d\n", n1 );
   printf("After swap, value of n2 : %d\n", n2);
 
   return 0;
}
void swap(int *x, int *y) {

   int temp;
   temp = *x;   
   *x   = *y;     
   *y   = temp;   
  
   return temp;
}